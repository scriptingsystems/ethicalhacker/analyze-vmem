La empresa X se ha puesto en contacto contigo para realizar un trabajo forense sobre un incidente reciente que ocurrió. Uno de sus empleados había recibido un correo electrónico de un compañero de trabajo que apuntaba a un archivo PDF. Al abrir, el empleado no notó nada; sin embargo, recientemente tuvieron actividad inusual en su cuenta bancaria.

La teoría inicial es que un usuario recibió un correo electrónico que contenía una URL que conducía a un documento PDF falsificado. Al abrir ese documento en Acrobat Reader se activa un Javascript malicioso que inicia una secuencia de acciones para apoderarse del sistema de la víctima.

La empresa X pudo obtener una imagen de la memoria de la máquina virtual del empleado ante la sospecha de infección y te pidió, como analista del blue team de seguridad, que analizaras la memoria virtual y proporcionaras respuestas a las preguntas.

1. ¿Cuál era la dirección IP local de la máquina de la víctima?
2. ¿Cuál era el valor de la variable de entorno del sistema operativo?
3. ¿Cuál era la contraseña del administrador?
4. ¿Qué proceso fue más probablemente responsable del exploit inicial?
5. ¿Cuál es la extensión del archivo malicioso recuperado del proceso responsable del exploit inicial?
6. Los procesos sospechosos abrieron conexiones de red a IP externas. Uno de ellos comienza con "2". Proporcione la IP completa.
7. Una URL sospechosa estaba presente en la memoria del proceso svchost.exe. Proporcione la URL completa que apunta a una página PHP alojada en una IP pública (sin FQDN).
8. Extraiga archivos del proceso inicial. Un archivo tiene un hash MD5 que termina en "528afe08e437765cc". ¿Cuándo se envió este archivo por primera vez para su análisis en VirusTotal?
9. ¿Cuál fue el PID del proceso que cargó el archivo PDF.php?
10. El JS incluye una función destinada a ocultar la llamada a la función eval(). Proporcione el nombre de esa función.
11. La carga útil incluye 3 códigos shell para diferentes versiones de Acrobat Reader. Proporcione el nombre de la función que corresponda a Acrobat v9.
12. El proceso winlogon.exe alojó un malware popular que se envió por primera vez para su análisis en VirusTotal el 29 de marzo de 2010 a las 11:34:01. Proporcione el hash MD5 de ese malware.
13. ¿Cuál es el nombre del ejecutable malicioso al que se hace referencia en el subárbol del registro '\WINDOWS\system32\config\software' y es una variante del troyano ZeuS?
14. El código shell para Acrobat v7 descarga un archivo llamado e.exe desde una URL específica. Proporcione la URL.
15. El código shell para Acrobat v8 explota una vulnerabilidad específica. Proporcione el número CVE.



## 1. ¿Cuál era la dirección IP local de la máquina de la víctima?

Nos descargamos volatility Foundation de la siguiente fuente:
Fuente: https://www.volatilityfoundation.org/releases

Descomprimimos y ponemos el binario a disponicion para utilizarlo desde cualquier path, por ejemplo **/usr/local/bin**

Obtenemos información del dump de la memoria, y nos quedamos con su **profile**, siendo **WinXPSP2x86**
```bash
$ volatility -f bank.vmem imageinfo
Volatility Foundation Volatility Framework 2.6
INFO    : volatility.debug    : Determining profile based on KDBG search...
          Suggested Profile(s) : WinXPSP2x86, WinXPSP3x86 (Instantiated with WinXPSP2x86)
                     AS Layer1 : IA32PagedMemoryPae (Kernel AS)
                     AS Layer2 : FileAddressSpace (/home/alice/Escritorio/bank/bank.vmem)
                      PAE type : PAE
                           DTB : 0x319000L
                          KDBG : 0x80544ce0L
          Number of Processors : 1
     Image Type (Service Pack) : 2
                KPCR for CPU 0 : 0xffdff000L
             KUSER_SHARED_DATA : 0xffdf0000L
           Image date and time : 2010-02-27 20:12:38 UTC+0000
     Image local date and time : 2010-02-27 15:12:38 -0500
```

Mostramos las conexiones, siendo la dirección IP la máquina la **192.168.0.176**
```bash
$ volatility -f bank.vmem --profile=WinXPSP2x86 connections
Volatility Foundation Volatility Framework 2.6
Offset(V)  Local Address             Remote Address            Pid
---------- ------------------------- ------------------------- ---
0x81c6a9f0 192.168.0.176:1176        212.150.164.203:80        888
0x82123008 192.168.0.176:1184        193.104.22.71:80          880
0x81cd4270 192.168.0.176:2869        192.168.0.1:30379         1244
0x81e41108 127.0.0.1:1168            127.0.0.1:1169            888
0x8206ac58 127.0.0.1:1169            127.0.0.1:1168            888
0x82108890 192.168.0.176:1178        212.150.164.203:80        1752
0x82210440 192.168.0.176:1185        193.104.22.71:80          880
0x8207ac58 192.168.0.176:1171        66.249.90.104:80          888
0x81cef808 192.168.0.176:2869        192.168.0.1:30380         4
0x81cc57c0 192.168.0.176:1189        192.168.0.1:9393          1244
0x8205a448 192.168.0.176:1172        66.249.91.104:80          888
```

## 2. ¿Cuál era el valor de la variable de entorno del sistema operativo?

Siendo la variable de entorno de SO, **OS Windows_NT**
```bash
$ volatility -f bank.vmem --profile=WinXPSP2x86 envars
     888 firefox.exe          0x00010000 NUMBER_OF_PROCESSORS           1
     888 firefox.exe          0x00010000 OS                             Windows_NT
     888 firefox.exe          0x00010000 Path                           C:\Program Files\Mozilla Firefox\;C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem
```

## 3. ¿Cuál era la contraseña del administrador?
```bash
$ volatility -f bank.vmem --profile=WinXPSP2x86 hashdump
Volatility Foundation Volatility Framework 2.6
Administrator:500:e52cac67419a9a224a3b108f3fa6cb6d:8846f7eaee8fb117ad06bdd830b7586c:::
<omitting output...>
```
**Crackstation.net**, Comprobar si ese hash si ha sido decifrado y publicado. En esa Web ponemos **8846f7eaee8fb117ad06bdd830b7586c** y dara como resultado de contraseña **password** 


## 4. ¿Qué proceso fue más probablemente responsable del exploit inicial?

Verificamos la lista de proccess
```
$ volatility -f bank.vmem psscan
Volatility Foundation Volatility Framework 2.6
Offset(P)          Name                PID   PPID PDB        Time created                   Time exited                   
------------------ ---------------- ------ ------ ---------- ------------------------------ ------------------------------
0x0000000001e80c78 wuauclt.exe         440   1040 0x04040240 2010-02-27 19:48:49 UTC+0000                                 
0x0000000001ea96f0 VMwareTray.exe     1108   1756 0x04040180 2010-02-26 03:34:39 UTC+0000                                 
0x0000000001edd790 explorer.exe       1756   1660 0x04040260 2010-02-26 03:34:38 UTC+0000                                 
0x0000000001ee1af8 msiexec.exe         452    244 0x04040320 2010-02-26 03:46:07 UTC+0000   2010-02-26 03:46:28 UTC+0000  
0x0000000001eee5f8 wscntfy.exe        1132   1040 0x040402a0 2010-02-26 03:34:40 UTC+0000                                 
0x0000000001f3f020 vmacthlp.exe        852    688 0x040400c0 2010-02-26 03:34:06 UTC+0000                                 
0x0000000001fdd8d0 VMUpgradeHelper    1836    688 0x040401e0 2010-02-26 03:34:34 UTC+0000                                 
0x0000000001fde568 spoolsv.exe        1460    688 0x040401a0 2010-02-26 03:34:10 UTC+0000                                 
0x0000000001fe55f0 svchost.exe        1244    688 0x04040160 2010-02-26 03:34:08 UTC+0000                                 
0x0000000001fea020 svchost.exe        1100    688 0x04040140 2010-02-26 03:34:07 UTC+0000                                 
0x000000000205b2e8 winlogon.exe        644    548 0x04040060 2010-02-26 03:34:04 UTC+0000                                 
0x0000000002104228 smss.exe            548      4 0x04040020 2010-02-26 03:34:02 UTC+0000                                 
0x00000000022618c8 AcroRd32.exe       1752    888 0x04040300 2010-02-27 20:12:23 UTC+0000                                 
0x0000000002268020 firefox.exe         888   1756 0x04040380 2010-02-27 20:11:53 UTC+0000                                 
0x00000000022cd5c8 VMwareUser.exe     1116   1756 0x04040280 2010-02-26 03:34:39 UTC+0000                                 
0x00000000022d6b88 alg.exe            2024    688 0x04040200 2010-02-26 03:34:35 UTC+0000                                 
0x00000000023018b0 vmtoolsd.exe       1628    688 0x040401c0 2010-02-26 03:34:25 UTC+0000                                 
0x0000000002329da0 lsass.exe           700    644 0x040400a0 2010-02-26 03:34:06 UTC+0000                                 
0x0000000002409640 svchost.exe        1384    688 0x040402e0 2010-02-27 20:12:36 UTC+0000                                 
0x000000000241a020 wuauclt.exe         232   1040 0x04040220 2010-02-27 19:49:11 UTC+0000                                 
0x0000000002456da0 services.exe        688    644 0x04040080 2010-02-26 03:34:05 UTC+0000                                 
0x0000000002466870 svchost.exe         880    688 0x040400e0 2010-02-26 03:34:07 UTC+0000                                 
0x00000000024e1da0 svchost.exe         948    688 0x04040100 2010-02-26 03:34:07 UTC+0000                                 
0x00000000024ea020 svchost.exe        1040    688 0x04040120 2010-02-26 03:34:07 UTC+0000                                 
0x00000000024eeda0 csrss.exe           612    548 0x04040040 2010-02-26 03:34:04 UTC+0000                                 
0x0000000002533620 msiexec.exe         244    688 0x040402c0 2010-02-26 03:46:06 UTC+0000                                 
0x00000000025c8830 System                4      0 0x00319000  
```

Segun lo que nos comentan 
> Uno de sus empleados había recibido un correo electrónico de un compañero de trabajo que apuntaba a un archivo PDF. Al abrir, el empleado no notó nada; sin embargo, recientemente tuvieron actividad inusual en su cuenta bancaria.

Comprobamos que la aplicacion que trata los PDFs es **AcroRd32.exe**, siendo su PID **1752** y con PPID **888**. Siendo el PPID asociado la aplicación de **firefox.exe**


## 5. ¿Cuál es la extensión del archivo malicioso recuperado del proceso responsable del exploit inicial?

as the Sus process was Acrobat the extionstion os the file must be **pdf**

## 6. Los procesos sospechosos abrieron conexiones de red a IP externas. Uno de ellos comienza con "2". Proporcione la IP completa.

Verificamos las conexiones
```bash
$ volatility -f bank.vmem --profile=WinXPSP2x86 connections
Volatility Foundation Volatility Framework 2.6
Offset(V)  Local Address             Remote Address            Pid
---------- ------------------------- ------------------------- ---
0x81c6a9f0 192.168.0.176:1176        212.150.164.203:80        888
0x82123008 192.168.0.176:1184        193.104.22.71:80          880
0x81cd4270 192.168.0.176:2869        192.168.0.1:30379         1244
0x81e41108 127.0.0.1:1168            127.0.0.1:1169            888
0x8206ac58 127.0.0.1:1169            127.0.0.1:1168            888
0x82108890 192.168.0.176:1178        212.150.164.203:80        1752
0x82210440 192.168.0.176:1185        193.104.22.71:80          880
0x8207ac58 192.168.0.176:1171        66.249.90.104:80          888
0x81cef808 192.168.0.176:2869        192.168.0.1:30380         4
0x81cc57c0 192.168.0.176:1189        192.168.0.1:9393          1244
0x8205a448 192.168.0.176:1172        66.249.91.104:80          888
```

Localizamos la conexión que relaciona al PID y el PPID del proceso, siendo PID:**1752** y PPID:**888**
```bash
0x82108890 192.168.0.176:1178        212.150.164.203:80        1752
0x81c6a9f0 192.168.0.176:1176        212.150.164.203:80        888
0x8207ac58 192.168.0.176:1171        66.249.90.104:80          888
0x8205a448 192.168.0.176:1172        66.249.91.104:80          888
```

## 7. Una URL sospechosa estaba presente en la memoria del proceso svchost.exe. Proporcione la URL completa que apunta a una página PHP alojada en una IP pública (sin FQDN).



```bash
$ volatility -f bank.vmem --profile=WinXPSP2x86 yarascan -Y 193.104.22.71
Rule: r1
Owner: Process svchost.exe Pid 880
0x000d8a1f  31 39 33 2e 31 30 34 2e 32 32 2e 37 31 2f 7e 70   193.104.22.71/~p
0x000d8a2f  72 6f 64 75 6b 74 2f 39 6a 38 35 36 66 5f 34 6d   rodukt/9j856f_4m
0x000d8a3f  39 79 38 75 72 62 2e 70 68 70 00 00 00 00 00 00   9y8urb.php......
0x000d8a4f  00 0a 00 08 00 b3 01 0c 00 55 73 65 72 2d 41 67   .........User-Ag
0x000d8a5f  65 6e 74 3a 20 4d 6f 7a 69 6c 6c 61 2f 34 2e 30   ent:.Mozilla/4.0
0x000d8a6f  20 28 63 6f 6d 70 61 74 69 62 6c 65 3b 20 4d 53   .(compatible;.MS
0x000d8a7f  49 45 20 36 2e 30 3b 20 57 69 6e 64 6f 77 73 20   IE.6.0;.Windows.
0x000d8a8f  4e 54 20 35 2e 31 3b 20 53 56 31 29 00 00 00 00   NT.5.1;.SV1)....
0x000d8a9f  00 0b 00 0a 00 ad 01 08 00 a8 8a 0d 00 a8 8a 0d   ................
0x000d8aaf  00 08 00 00 00 18 9f 0d 00 31 00 00 00 58 ea 0b   .........1...X..
0x000d8abf  00 0d 00 00 00 00 00 6f 00 08 00 00 00 00 00 00   .......o........
0x000d8acf  00 00 00 00 00 00 00 00 00 00 00 67 00 00 00 00   ...........g....
0x000d8adf  00 04 00 00 00 03 00 00 00 00 00 00 00 00 00 00   ................
0x000d8aef  00 02 00 00 00 c2 00 00 00 04 00 0b 00 a6 01 0c   ................
0x000d8aff  00 43 6f 6e 74 65 6e 74 2d 4c 65 6e 67 74 68 3a   .Content-Length:
0x000d8b0f  20 33 37 33 00 00 00 00 00 04 00 04 00 9a 01 09   .373............
```


```bash
$ strings bank.vmem |grep "^http://"|sort|uniq |grep "php"
http://193.104.22.71/~produkt/9j856f_4m9y8urb.php
http://193.104.22.71/~produkt/9j856f_4m9y8urb.php&N
http://search-network-plus.com/cache/PDF.php?st=Internet%20Explorer%206.0
http://search-network-plus.com/load.php?a=a&st=Internet Explorer 6.0&e=1
http://search-network-plus.com/load.php?a=a&st=Internet Explorer 6.0&e=2
http://search-network-plus.com/load.php?a=a&st=Internet Explorer 6.0&e=3
http://search-network-plus.com/load.php?a=a&st=Internet%20Explorer%206.0&e=2
```

hell yeah it worked :LOL: the url was
```bash
http://193.104.22.71/~produkt/9j856f_4m9y8urb.php
```


## 8. Extraiga archivos del proceso inicial. Un archivo tiene un hash MD5 que termina en "528afe08e437765cc". ¿Cuándo se envió este archivo por primera vez para su análisis en VirusTotal?

Hacemos un dump del proceso **AcroRd32** siendo el **1752**
```bash
$ volatility -f bank.vmem --profile=WinXPSP2x86 memdump -p 1752 -D PID1752
Volatility Foundation Volatility Framework 2.6
************************************************************************
Writing AcroRd32.exe [  1752] to 1752.dmp

$ ll PID1752/
total 326396
drwxrwxr-x 2 alice alice      4096 nov 29 13:29 ./
drwxrwxr-x 7 alice alice      4096 nov 29 13:28 ../
-rw-rw-r-- 1 alice alice 334217216 nov 29 13:29 1752.dmp
```

then use foremost to extract the content **foremost**
```bash
$ foremost -i PID1752/1752.dmp -o OUTFILE1752/
OR
$ foremost -t pdf -i PID1752/1752.dmp -o OUTFILE1752/

$ ll OUTFILE1752/pdf/
total 684
drwxrwxr--  2 alice alice   4096 nov 29 13:33 ./
drwxrwxr-x 13 alice alice   4096 nov 29 13:33 ../
-rw-rw-r--  1 alice alice    419 nov 29 13:33 00445397.pdf
-rw-rw-r--  1 alice alice    419 nov 29 13:33 00446730.pdf
-rw-rw-r--  1 alice alice    425 nov 29 13:33 00579981.pdf
-rw-rw-r--  1 alice alice    425 nov 29 13:33 00585184.pdf
-rw-rw-r--  1 alice alice    425 nov 29 13:33 00600544.pdf
-rw-rw-r--  1 alice alice  60124 nov 29 13:33 00600928.pdf
-rw-rw-r--  1 alice alice 607083 nov 29 13:33 00601560.pdf


$ md5sum OUTFILE1752/pdf/*
4c1d41459dc5b476394ceb06471dd1f8  OUTFILE1752/pdf/00445397.pdf
4c1d41459dc5b476394ceb06471dd1f8  OUTFILE1752/pdf/00446730.pdf
1873297b6b3702817a760f95187236bc  OUTFILE1752/pdf/00579981.pdf
1873297b6b3702817a760f95187236bc  OUTFILE1752/pdf/00585184.pdf
1873297b6b3702817a760f95187236bc  OUTFILE1752/pdf/00600544.pdf
70ebcd37c81e49858b8946ba49eb44b5  OUTFILE1752/pdf/00600928.pdf
f32aa81676c7391528afe08e437765cc  OUTFILE1752/pdf/00601560.pdf
```

and the answer was

> 2010-03-29 19:31:45

## 9. ¿Cuál fue el PID del proceso que cargó el archivo PDF.php?
**AcroRd32.exe** so the pid will be
> 1752

## 10. El JS incluye una función destinada a ocultar la llamada a la función eval(). Proporcione el nombre de esa función.
We already know the sus pdf so I am going to use **peepdf** tool it will help a lot
**peepdf**: https://github.com/jesparza/peepdf


```bash
$ python peepdf.py 00601560.pdf
$ echo 'extract js > all-javascripts-from-my.pdf' > xtract.txt 
$ python peepdf.py -l -f -s xtract.txt 00601560.pdf
$ cat all-javascripts-from-my.pdf
```

```json
HNQYxrFW(eval, VIfwHVPz(xtdxJYVm, JkYBYnxN), BGmiwYYc);

function HYOtmIjW(DTBYIswO, BEundbzB) {
    return (DTBYIswO || BEundbzB) && !(DTBYIswO && BEundbzB);
}
function VIfwHVPz(xtdxJYVm, JkYBYnxN) {
    return SvaHZsuK(GcBigPkz(JkYBYnxN), GcBigPkz(xtdxJYVm))
}
function vrfDJomH(ENzEszAz) {
    return (ENzEszAz == 1) ? true : false;
}
function GcBigPkz(xtdxJYVm) {
    return xtdxJYVm;
}
function Dqakslkn(ENzEszAz, Dqakslkn) {
    if (Dqakslkn == 0) {
        return 1;
    }
    var VzBJVOyp = ENzEszAz;
    for (var GlyomGyU = 1; GlyomGyU < Dqakslkn; GlyomGyU++) {
        VzBJVOyp *= ENzEszAz;
```


## 11. La carga útil incluye 3 códigos shell para diferentes versiones de Acrobat Reader. Proporcione el nombre de la función que corresponda a Acrobat v9. PENDING...

My first extraction wasn’t complete so i will try another tool called pdf-pareser
**pdf-pareser**: https://github.com/smalot/pdfparser


```bash
$ python pdf-parser.py --search javascript --raw 00601560.pdf
$ python pdf-parser.py --object 11 00601560.pdf
$ python pdf-parser.py --object 1054 --raw --filter 00601560.pdf > malicious.js
$ js malicious.js 
```

## 12. El proceso winlogon.exe alojó un malware popular que se envió por primera vez para su análisis en VirusTotal el 29 de marzo de 2010 a las 11:34:01. Proporcione el hash MD5 de ese malware. PENDING....

Fuente: https://mmox.me/posts/writeups/bankingtroubles-memory-image-forensics/
